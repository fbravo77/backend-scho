package com.example.educapp.security.entity;

import com.example.educapp.core.models.persistence.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "USERS")
public class User extends BaseEntity {

    @NotNull
    private String userName;

    @NotNull
    private String password;

    private boolean isActive;

    @NotNull
    @ManyToMany (fetch= FetchType.EAGER)
    @JoinTable(name="role_user", joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name="role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(@NotNull String userName, @NotNull String password, boolean isActive) {
        this.userName = userName;
        this.password = password;
        this.isActive = isActive;
    }

    public User() {
    }
}
