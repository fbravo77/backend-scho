package com.example.educapp.security.entity;

import com.example.educapp.core.models.persistence.BaseEntity;
import com.example.educapp.security.enums.RoleName;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "ROLES")
public class Role extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleName roleName;

}
