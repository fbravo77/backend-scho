package com.example.educapp.security.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PrincipalUser implements UserDetails {

    private String email;

    private String password;

    private String userName;

    private boolean isActive;

    private Collection <? extends GrantedAuthority> authorities;

    public PrincipalUser() {}

    public static PrincipalUser build (User user){
        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(rol -> new SimpleGrantedAuthority(rol
                        .getRoleName()
                        .name())).collect(Collectors.toList());

        return new PrincipalUser(
                                user.getPassword(),
                                user.getUserName(),
                                user.isActive(),
                                authorities);
    }

    public PrincipalUser(String password, String userName, boolean isActive, Collection<? extends GrantedAuthority> authorities) {
        this.password = password;
        this.userName = userName;
        this.isActive = isActive;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
