package com.example.educapp.security.enums;

public enum RoleName {
    ROLE_ADMIN, ROLE_COORDINATOR, ROLE_TEACHER, ROLE_PARENT, ROLE_STUDENT
}
