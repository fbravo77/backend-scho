package com.example.educapp.security.service;

import com.example.educapp.security.entity.Role;
import com.example.educapp.security.enums.RoleName;
import com.example.educapp.security.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RoleService {

    @Autowired
    RolRepository rolRepository;

    public Optional<Role> getRoleByName (RoleName roleName) {
        return rolRepository.findByRoleName(roleName);
    }
}
