package com.example.educapp.security.service;

import com.example.educapp.security.entity.User;
import com.example.educapp.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository userRepository;

    public Optional<User> getByUserName(String userName){
        return userRepository.findByUserName(userName);
    }

    public boolean existByUserName(String userName){
        return userRepository.existsByUserName(userName);
    }

    public void save (User user){
        userRepository.save(user);
    }
}
