package com.example.educapp.security.repository;

import com.example.educapp.security.entity.Role;
import com.example.educapp.security.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByRoleName(RoleName roleName);
}
