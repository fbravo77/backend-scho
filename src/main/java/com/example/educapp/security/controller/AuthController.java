package com.example.educapp.security.controller;

import com.example.educapp.security.dto.JwtDto;
import com.example.educapp.security.dto.LoginUser;
import com.example.educapp.security.entity.Role;
import com.example.educapp.security.entity.User;
import com.example.educapp.security.enums.RoleName;
import com.example.educapp.security.jwt.JwtProvider;
import com.example.educapp.security.service.RoleService;
import com.example.educapp.security.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    private static final Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/new")
    public ResponseEntity<?> nuevo(){

        User user = new User("hola@hola.com",passwordEncoder.encode("123456"),true);
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getRoleByName(RoleName.ROLE_ADMIN).get());

        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity("Exito", HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDto> login (@Valid @RequestBody LoginUser loginUser){

        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUserName(), loginUser.getPassword()));



            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtProvider.generateToken(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();

            JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
            return new ResponseEntity(jwtDto, HttpStatus.OK);
        }
        catch (Exception e){
            logger.error("ERROR: " + e.getMessage());
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }


}
