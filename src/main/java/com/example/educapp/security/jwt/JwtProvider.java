package com.example.educapp.security.jwt;

import com.example.educapp.security.entity.PrincipalUser;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

//GENERA EL TOKEN, VALIDA QUE ESTE BIEN FORMADO, QUE NO ESTE EXPIRADO, ETC
@Component
public class JwtProvider {
    private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private int expiration;

    public String generateToken(Authentication authentication) {

        //To Manage the expiration of token
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, expiration);

        PrincipalUser principalUser = (PrincipalUser) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(principalUser.getUsername())
                .setIssuedAt(currentDate)
                .setExpiration(c.getTime())
                .signWith(SignatureAlgorithm.HS512 , secret)
                        .compact();

    }
    public String getUserNameFromToken (String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }
    public boolean validateToken (String token){
        try{
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        }
        catch(MalformedJwtException e){
            logger.error("Token mal formado");
        }
        catch(UnsupportedJwtException e){
            logger.error("Token no soportado");
        }
        catch(IllegalArgumentException e){
            logger.error("Token vacio");
        }
        catch(ExpiredJwtException e){
            logger.error("Token expirado");
        }
        catch(SignatureException e){
            logger.error("Token mal firmado");
        }
        return false;
    }

}
