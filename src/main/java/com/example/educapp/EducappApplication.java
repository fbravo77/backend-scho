package com.example.educapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EducappApplication {

    public static void main(String[] args) {
        SpringApplication.run(EducappApplication.class, args);
    }
}
