package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Student;
import com.example.educapp.core.models.persistence.Teacher;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentRepository extends PagingAndSortingRepository<Student, Long>, QuerydslPredicateExecutor<Student> {

    List<Student> findByIsActiveAndYear(boolean active, Year year);

    List<Student> findByFirstNameLikeOrLastNameLikeAndIsActiveAndYear(String firstName,String lastName, boolean active, Year year);

    List<Student> findByGenderAndIsActiveAndYear(String gender,boolean active, Year year);

}
