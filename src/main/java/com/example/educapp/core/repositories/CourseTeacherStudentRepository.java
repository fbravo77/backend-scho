package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.CourseTeacherStudent;
import com.example.educapp.core.models.persistence.Grade;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CourseTeacherStudentRepository extends PagingAndSortingRepository<CourseTeacherStudent, Long>, QuerydslPredicateExecutor<CourseTeacherStudent> {
}
