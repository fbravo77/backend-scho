package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Course;
import com.example.educapp.core.models.persistence.Grade;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {

    Page<Course> findByNameContainingIgnoreCaseAndIsActiveAndYear(String name, boolean active, Year year, Pageable pageable);

    Page<Course> findByIsActive(boolean active, Pageable pageable);

    Page<Course> findByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Course> findByYear(Year year, Pageable pageable);

    Page<Course> findByNameContainingIgnoreCaseAndIsActive(String name,boolean active, Pageable pageable);

    Page<Course> findByIsActiveAndYear(boolean active,Year year, Pageable pageable);

    Page<Course> findByNameContainingIgnoreCaseAndYear(String name,Year year, Pageable pageable);

    List<Course> findByYearAndIsActive(Year year, boolean isActive);



}
