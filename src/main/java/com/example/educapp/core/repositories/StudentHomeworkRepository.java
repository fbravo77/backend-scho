package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.StudentHomeworks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentHomeworkRepository extends JpaRepository<StudentHomeworks, Long> {
}
