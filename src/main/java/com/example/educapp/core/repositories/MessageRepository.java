package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.GradeSection;
import com.example.educapp.core.models.persistence.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByGradeSectionOrGradeSectionIsNullAndDateAfter(GradeSection gradeSection, Date date);

    List<Message> findByDateAfterAndGradeSectionIsNull(Date date);

}
