package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Grade;
import com.example.educapp.core.models.persistence.Section;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SectionRepository extends JpaRepository<Section, Long> {

    Page<Section> findByNameContainingIgnoreCaseAndIsActiveAndYear(String name, boolean active, Year year, Pageable pageable);

    Page<Section> findByIsActiveAndYear(boolean active, Year year, Pageable pageable);

    Page<Section> findByNameContainingIgnoreCaseAndIsActive(String name, boolean active, Pageable pageable);

    Page<Section> findByNameContainingIgnoreCaseAndYear(String name, Year year, Pageable pageable);

    Page<Section> findByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Section> findByIsActive(boolean active, Pageable pageable);

    Page<Section> findByYear(Year year, Pageable pageable);

    List<Section> findByIsActiveAndYear(boolean active,Year year);

}
