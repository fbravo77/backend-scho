package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Gestiona la capa de acceso a datos de la tabla year.
 * @author Francisco Bravo
 *
 */
public interface YearRepository extends JpaRepository<Year, Long> {

    Page<Year> findByYearAndIsActive(int year, boolean active, Pageable pageable);

    Page<Year> findByIsActive(boolean active, Pageable pageable);

    Page<Year> findByYear(int year,Pageable pageable);

    Optional<Year> findByYear(int year);

    Optional<Year> findByIsActive(boolean active);
}
