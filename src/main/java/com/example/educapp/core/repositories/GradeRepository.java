package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Grade;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Gestiona la capa de acceso a datos de la tabla grades.
 * @author Francisco Bravo
 *
 */
public interface GradeRepository extends JpaRepository<Grade, Long> {

    Page<Grade> findByNameLikeAndIsActiveAndYear(String name, boolean active,Year year, Pageable pageable);

    Page<Grade> findByNameLikeAndIsActive(String name, boolean active, Pageable pageable);

    Page<Grade> findByNameLike(String name, Pageable pageable);

    Page<Grade> findByIsActive(boolean active, Pageable pageable);

    Page<Grade> findByYear(Year year, Pageable pageable);

    Page<Grade> findByYearAndIsActive(Year year,boolean active, Pageable pageable);

    Page<Grade> findByNameLikeAndYear(String name,Year year, Pageable pageable);

    List<Grade> findByYearAndIsActive(Year year, boolean active);

}
