package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Grade;
import com.example.educapp.core.models.persistence.GradeSection;
import com.example.educapp.core.models.persistence.Section;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GradeSectionRepository extends JpaRepository<GradeSection, Long> {

    List<GradeSection> findByIsActive(boolean active);

    List<GradeSection> findByGradeAndSection(Grade grade, Section section);

    Optional<GradeSection> findFirstByGradeAndSection(Grade grade, Section section);

    List<GradeSection> findByGradeAndIsActive(Grade grade,boolean active);

}
