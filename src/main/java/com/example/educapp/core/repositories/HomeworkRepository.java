package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.CourseTeacher;
import com.example.educapp.core.models.persistence.Homework;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface HomeworkRepository extends PagingAndSortingRepository<Homework, Long>, QuerydslPredicateExecutor<CourseTeacher> {

    List<Homework> findByNameLikeOrDetailLike(String name, String detail);

    List<Homework> findByScore(double score);
}
