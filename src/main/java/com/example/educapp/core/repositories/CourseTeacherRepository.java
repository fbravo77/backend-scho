package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.CourseTeacher;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CourseTeacherRepository extends PagingAndSortingRepository<CourseTeacher, Long>, QuerydslPredicateExecutor<CourseTeacher> {

}
