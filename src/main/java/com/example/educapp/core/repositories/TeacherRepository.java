package com.example.educapp.core.repositories;

import com.example.educapp.core.models.persistence.Section;
import com.example.educapp.core.models.persistence.Student;
import com.example.educapp.core.models.persistence.Teacher;
import com.example.educapp.core.models.persistence.Year;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface TeacherRepository extends PagingAndSortingRepository<Teacher, Long>, QuerydslPredicateExecutor<Teacher> {



    Optional<Teacher> findByIsActiveAndYearAndContactEmail(boolean active, Year year, String contactMail);
/*
    List<Teacher> findByIsActiveAndYear(boolean active, Year year);

    Page<Teacher> findByYear(Year year, Pageable pageable);

    Page<Teacher> findByIsActive(boolean active, Pageable pageable);

    Page<Teacher> findByGender(String gender, Pageable pageable);

    Page<Teacher> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    Page<Teacher> findByFirstNameContainingIgnoreCase(String firstName, Pageable pageable);

    /* MIX */
/*    Page<Teacher> findByYearAndIsActive(Year year,boolean active, Pageable pageable);

    Page<Teacher> findByYearAndGender(Year year,String gender, Pageable pageable);

    Page<Teacher> findByYearAndLastNameContainingIgnoreCase(Year year,String lastName, Pageable pageable);

    Page<Teacher> findByYearAndFirstNameContainingIgnoreCase(Year year,String firstName, Pageable pageable);

    Page<Teacher> findByIsActiveAndGender(boolean active, String gender,Pageable pageable);

    Page<Teacher> findByIsActiveAndLastNameContainingIgnoreCase(boolean active, String lastName, Pageable pageable);

    Page<Teacher> findByIsActiveAndFirstNameContainingIgnoreCase(boolean active, String firstName, Pageable pageable);

    Page<Teacher> findByGenderAndLastNameContainingIgnoreCase(String gender,String lastName, Pageable pageable);

    Page<Teacher> findByGenderAndFirstNameContainingIgnoreCase(String gender,String firstName, Pageable pageable);

    Page<Teacher> findByLastNameContainingIgnoreCaseAndFirstNameContainingIgnoreCase(String lastName,String firsName, Pageable pageable);
*/}
