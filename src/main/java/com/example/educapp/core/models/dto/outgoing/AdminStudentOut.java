package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;

@Data
public class AdminStudentOut {

    private long id;
    private String firstName;
    private String lastName;
    private String personCharge;
    private String otherPersonCharge;
    private String allergies;
    private String contactPhone1;
    private String contactPhone2;
    private String contactPhone3;
    private String gender;
    private String dateOfBirth;
    private String contactEmail;
    private String gradeAndSection;
    private boolean isActive;
}
