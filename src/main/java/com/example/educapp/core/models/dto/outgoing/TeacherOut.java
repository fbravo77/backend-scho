package com.example.educapp.core.models.dto.outgoing;

import com.example.educapp.core.models.persistence.Year;
import lombok.Data;

import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class TeacherOut {

    private String firstName;
    private String lastName;
    private String alergies;
    private String contactPhone1;
    private String contactPhone2;
    private String contactPhone3;
    private String gender;
    private Date dateBirth;
    private String contactEmail;
    private String year;
    private boolean isActive;
}
