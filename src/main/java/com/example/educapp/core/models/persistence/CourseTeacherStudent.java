package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "STUDENT_COURSES")
public class CourseTeacherStudent extends BaseEntity{

    @ManyToOne
    private CourseTeacher courseTeacher;

    @ManyToOne
    private Student student;

    private double score;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "homework")
    private List<StudentHomeworks> homeworkList;
}
