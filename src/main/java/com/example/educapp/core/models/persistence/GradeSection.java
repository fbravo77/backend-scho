package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "GRADE_SECTIONS")
public class GradeSection extends BaseEntity{

    @ManyToOne
    private Grade grade;

    @ManyToOne
    private Section section;

    @NotNull
    private boolean isActive;
}
