package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "COURSES")
public class Course extends BaseEntity{

    @NotEmpty
    @NotNull
    private String name;

    private String description;

    @ManyToMany(mappedBy = "courses")
    List<Grade> grades;

    @ManyToOne
    private Year year;

    @NotNull
    private boolean isActive;
}
