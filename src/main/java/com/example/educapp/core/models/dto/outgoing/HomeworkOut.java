package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;
import java.util.Date;

@Data
public class HomeworkOut {

    private Long id;
    private String name;
    private String detail;
    private double score;
    private Date deliveryDate;
    private String course;
    private String teacherName;
    private Long courseTeacherId;

    public HomeworkOut(Long id, String name, String detail, double score, Date deliveryDate, String course, String teacherName, Long courseTeacherId) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.score = score;
        this.deliveryDate = deliveryDate;
        this.course = course;
        this.teacherName = teacherName;
        this.courseTeacherId = courseTeacherId;
    }

    public HomeworkOut(){}

}
