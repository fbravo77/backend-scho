package com.example.educapp.core.models.dto.receive;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class GradeIn {

    private Long id;

    @NotEmpty
    private String name;

    @NotNull
    private boolean isActive;

    @NotEmpty
    private String courses;

    @NotEmpty
    private String sections;
}
