package com.example.educapp.core.models.dto.receive;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CourseIn {

    private Long id;

    @NotEmpty
    private String name;

    @NotNull
    private boolean isActive;

}