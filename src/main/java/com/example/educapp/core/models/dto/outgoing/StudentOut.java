package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
class CourseNoteStudentOut{
    private String courseName;
    private double courseNote;

    CourseNoteStudentOut(String courseName,Double courseNote){
        this.courseName = courseName;
        this.courseNote = courseNote;
    }
}

@Data
class homeworkList{
    private String homeworkCourse;
    private String homeworkName;

    homeworkList(String homeworkCourse,String homeworkName){
        this.homeworkCourse = homeworkCourse;
        this.homeworkName = homeworkName;
    }
}

@Data
public class StudentOut {

    private long id;

    private String completeName;

    private double average;

    private int pendingHomeworks;

    private int unreadMessages;

    List<CourseNoteStudentOut> courseNotes;

    ArrayList<homeworkList> homeworkList;

    public void setCourseNoteStudentOut(Map<String, Double> courseNoteMap){
        courseNotes = new ArrayList<>();
        courseNoteMap.forEach((s, s2) -> courseNotes.add(new CourseNoteStudentOut(s,s2)));
    }

    public void setHomeworkListStudentOut(Map<String, String> homeworkMap){
        homeworkMap.forEach((s, s2) -> homeworkList.add(new homeworkList(s,s2)));
    }

    public StudentOut(long id, String completeName, double average, int pendingHomeworks, int unreadMessages, List<CourseNoteStudentOut> courseNotes, ArrayList<com.example.educapp.core.models.dto.outgoing.homeworkList> homeworkList) {
        this.id = id;
        this.completeName = completeName;
        this.average = average;
        this.pendingHomeworks = pendingHomeworks;
        this.unreadMessages = unreadMessages;
        this.courseNotes = courseNotes;
        this.homeworkList = homeworkList;
    }

    public StudentOut() {
    }
}
