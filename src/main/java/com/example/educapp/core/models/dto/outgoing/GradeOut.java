package com.example.educapp.core.models.dto.outgoing;

import com.example.educapp.core.models.persistence.Course;
import lombok.Data;
import java.util.*;

@Data
public class GradeOut {

    /**
     *
     * @param id id
     * @param gradeName gradeName
     * @param year year
     * @param active active
     */
    public GradeOut(long id, String gradeName, int year, boolean active, List<Course> coursesIn,String[] sections) {
        this.id = id;
        this.gradeName = gradeName;
        this.year = year;
        this.active = active;
            if(sections != null && sections.length>0) {
                this.sections = Arrays.stream(sections).mapToInt(Integer::parseInt).toArray();
            }
            if(coursesIn != null && !coursesIn.isEmpty()) {
                this.courses = new ArrayList<>();
                coursesIn.forEach(course ->
                        {
                            CourseGrade courseGrade = new CourseGrade(course.getId(), course.getName());
                            this.courses.add(courseGrade);
                        }
                );
            }/*
        for( Course course : coursesIn){
            CourseGrade courseGrade = new CourseGrade(course.getId(),course.getName());
            this.courses.add(courseGrade);
        }*/

    }

    private long id;

    private String gradeName;

    private int year;

    private boolean active;

    List<CourseGrade> courses;

    private int[] sections;
}

    @Data
    class CourseGrade{
        private long id;
        private String courseName;

        CourseGrade(long id,String courseName){
            this.id = id;
            this.courseName = courseName;
        }
    }



