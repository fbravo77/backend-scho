package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;

@Data
public class SectionOut {

    private long id;

    private String sectionName;

    private int year;

    public SectionOut(long id, String sectionName, int year, boolean active) {
        this.id = id;
        this.sectionName = sectionName;
        this.year = year;
        this.active = active;
    }

    private boolean active;
}
