package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "HOMEWORKS")
public class Homework extends BaseEntity{

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    @Column(length=8000)
    private String detail;

    @NotNull
    private double score;

    private Date deliveryDate;

    @NotNull
    @ManyToOne
    private CourseTeacher courseTeacher;

    public Homework(@NotNull @NotEmpty String name, @NotNull @NotEmpty String detail, @NotNull double score, Date deliveryDate, @NotNull CourseTeacher courseTeacher) {
        this.name = name;
        this.detail = detail;
        this.score = score;
        this.deliveryDate = deliveryDate;
        this.courseTeacher = courseTeacher;
    }

    public Homework(){};


}
