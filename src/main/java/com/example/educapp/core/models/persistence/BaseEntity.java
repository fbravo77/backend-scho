package com.example.educapp.core.models.persistence;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

/**
 * BASE ENTITY FOR ALL OTHER ENTITIES
 */
@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "id", updatable = false, nullable = false)
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
