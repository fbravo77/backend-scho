package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "COURSES_TEACHER")
public class CourseTeacher extends BaseEntity{

    @ManyToOne
    private GradeSection gradeSection;

    @ManyToOne
    private Teacher teacher;

    @ManyToOne
    private Course course;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "courseTeacher")
    private List<Homework> homeworkList;

    public CourseTeacher(GradeSection gradeSection, Teacher teacher, Course course) {
        this.gradeSection = gradeSection;
        this.teacher = teacher;
        this.course = course;
    }
    public CourseTeacher(){}
}
