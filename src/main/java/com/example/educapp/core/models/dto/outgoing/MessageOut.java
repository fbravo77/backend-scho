package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;
import java.util.Date;

@Data
public class MessageOut {

    private String subject;
    private String message;
    private Date date;
}
