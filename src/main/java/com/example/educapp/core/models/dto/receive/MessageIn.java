package com.example.educapp.core.models.dto.receive;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class MessageIn {

    @NotEmpty
    @NotNull
    private String subject;
    @NotEmpty
    @NotNull
    private String message;

    private long gradeSectionId;

}
