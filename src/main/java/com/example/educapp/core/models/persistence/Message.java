package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "MESSAGES")
public class Message extends BaseEntity {

    @NotEmpty
    @NotNull
    private String subject;

    @NotEmpty
    @NotNull
    private String message;

    @NotNull
    private Date date;

    @ManyToOne
    private GradeSection gradeSection;








}
