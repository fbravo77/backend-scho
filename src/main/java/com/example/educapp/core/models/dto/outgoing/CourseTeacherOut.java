package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;

@Data
public class CourseTeacherOut {

    private long id;
    private long courseId;
    private long gradeSectionId;
    private String courseName;
    private String gradeName;
    private String sectionName;

    public CourseTeacherOut (){}

    public CourseTeacherOut(long id, long courseId, long gradeSectionId, String courseName, String gradeName, String sectionName) {
        this.id = id;
        this.courseId = courseId;
        this.gradeSectionId = gradeSectionId;
        this.courseName = courseName;
        this.gradeName = gradeName;
        this.sectionName = sectionName;
    }
}
