package com.example.educapp.core.models.persistence;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.example.educapp.security.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "STUDENTS")
public class Student extends BaseEntity {

    @NotEmpty
    @NotNull
    private String firstName;

    @NotEmpty
    @NotNull
    private String lastName;

    //TODO PHOTO URL

    private String personCharge;
    private String otherPersonCharge;
    private String allergies;
    @NotEmpty
    @Size(min = 7, max = 50)
    private String contactPhone1;
    @Size(min = 7, max = 50)
    private String contactPhone2;
    @Size(min = 7, max = 50)
    private String contactPhone3;

    @NotEmpty
    @Size(min = 1, max = 1) //M OR F; MALE OR FEMALE
    private String gender;

    private Date dateBirth;

    private String contactEmail;

    @ManyToOne
    private Year year;

    @ManyToOne
    private GradeSection gradeSection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<CourseTeacherStudent> courses;

    @ManyToOne
    private User user;

    @NotNull
    private boolean isActive;


}
