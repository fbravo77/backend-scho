package com.example.educapp.core.models.dto.receive;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class HomeworkIn {

    private long id;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @NotEmpty
    private String detail;
    @NotNull
    private double score;
    private Date deliveryDate;
    @NotNull
    private Long courseTeacherId;
}
