package com.example.educapp.core.models.persistence;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "GRADES")
public class Grade extends BaseEntity{

    @NotEmpty
    private String name;

    @NotNull
    private boolean isActive;

    @ManyToMany
    @JoinTable(
            name = "grade_course",
            joinColumns = @JoinColumn(name = "grade_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    List<Course> courses;

    @ManyToOne
    private Year year;


}


