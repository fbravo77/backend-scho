package com.example.educapp.core.models.dto.outgoing;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
class HomeworkDetailCourseOut{
    private String homeworkName;
    private double homeworkScore;

    HomeworkDetailCourseOut(String homeworkName,Double homeworkScore){
        this.homeworkName = homeworkName;
        this.homeworkScore = homeworkScore;
    }
}

@Data
public class CourseOut {

    private long id;
    private String name;
    private int year;
    private boolean active;

    public CourseOut() {

    }

    public CourseOut(long id, String name, int year,boolean active) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.active = active;

    }

    private double score;
    private double accumulatedScore;
    List<HomeworkDetailCourseOut> courseNotesDetail;
    private int pendingHomework;

    public void setCourseNoteStudentOut(Map<String, Double> courseNoteMap){
        courseNotesDetail = new ArrayList<>();
        courseNoteMap.forEach((s, s2) -> courseNotesDetail.add(new HomeworkDetailCourseOut(s,s2)));
    }
}
