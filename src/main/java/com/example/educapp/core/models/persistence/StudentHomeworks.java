package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "STUDENT_HOMEWORKS")
public class StudentHomeworks extends BaseEntity{

    @ManyToOne
    private CourseTeacherStudent studentCourse;

    private double score;

    private String annotation;

    @ManyToOne
    private Homework homework;
}
