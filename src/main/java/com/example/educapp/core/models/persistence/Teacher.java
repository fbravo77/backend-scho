package com.example.educapp.core.models.persistence;

import com.example.educapp.security.entity.User;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@QueryEntity
@Table(name = "TEACHERS")
public class Teacher extends BaseEntity{

    @NotEmpty
    @NotNull
    private String firstName;

    @NotEmpty
    @NotNull
    private String lastName;

    private String alergies;
    @NotEmpty
    @Size(min = 7, max = 50)
    private String contactPhone1;
    @Size(min = 7, max = 50)
    private String contactPhone2;
    @Size(min = 7, max = 50)
    private String contactPhone3;

    @NotEmpty
    @Size(min = 1, max = 1) //M OR F; MALE OR FEMALE
    private String gender;
    
    private Date dateBirth;

    @NotEmpty
    @NotNull
    private String contactEmail;

    @ManyToOne
    private Year year;

    @ManyToOne
    private User user;

    @NotNull
    private boolean isActive;
}
