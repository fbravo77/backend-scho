package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "YEARS")
public class Year extends BaseEntity{

    @NotNull
    @Min(value=2020)
    @Max(value=2080)
    @Column(unique = true)
    private int year;

    @NotNull
    private boolean isActive;


}
