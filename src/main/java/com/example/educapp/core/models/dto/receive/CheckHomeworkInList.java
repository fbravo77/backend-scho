package com.example.educapp.core.models.dto.receive;

import lombok.Data;
import java.util.List;

@Data
public class CheckHomeworkInList {

    private List<CheckHomeworkIn> checkHomeworkInList;

    @Data
    public static class CheckHomeworkIn {

        private double score;

        private String annotation;

        private long homeworkId;

        private long courseTeacherStudentId;

        public CheckHomeworkIn(double score, String annotation, long homeworkId, long courseTeacherStudentId) {
            this.score = score;
            this.annotation = annotation;
            this.homeworkId = homeworkId;
            this.courseTeacherStudentId = courseTeacherStudentId;
        }

        public CheckHomeworkIn() {
        }
    }

    public CheckHomeworkInList(List<CheckHomeworkIn> checkHomeworkInList) {
        this.checkHomeworkInList = checkHomeworkInList;
    }

    public CheckHomeworkInList() {
    }
}