package com.example.educapp.core.models.dto.receive;
import lombok.Data;
import java.util.List;

@Data
public class TeacherIn{

    private String firstName;
    private String lastName;
    private String alergies;
    private String contactPhone1;
    private String contactPhone2;
    private String contactPhone3;
    private String gender;
    private String dateOfBirth;
    private String contactEmail;
    private boolean isActive;
    private List<CourseTeacherIn> courses;

    @Data
    public static class CourseTeacherIn{
        private long gradeId;
        private long sectionId;
        private long courseId;

        public CourseTeacherIn() {
        }
    }
}
