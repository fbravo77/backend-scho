package com.example.educapp.core.models.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SECTIONS")
public class Section extends BaseEntity{

    @NotEmpty
    private String name;

    @NotNull
    private boolean isActive;

    @ManyToOne
    private Year year;
}
