package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.AdminStudentOut;
import com.example.educapp.core.models.dto.outgoing.StudentOut;
import com.example.educapp.core.models.dto.receive.StudentIn;
import com.example.educapp.core.models.persistence.CourseTeacherStudent;
import com.example.educapp.core.models.persistence.Student;
import com.example.educapp.core.models.persistence.StudentHomeworks;
import com.example.educapp.core.services.StudentService;
import com.example.educapp.core.services.YearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.*;

@RestController
@RequestMapping(path = "/student", produces = "application/json")
@CrossOrigin(origins = "*")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private YearService yearServ;

    /**
     * STUDENT ADMIN METHODS
     */
    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<AdminStudentOut>> getStudents(@RequestParam(name = "firstName", required = false) Optional<String> firstName,
                                                             @RequestParam(name = "lastName", required = false) Optional<String> lastName,
                                                             @RequestParam(name = "email", required = false) Optional<String> email,
                                                             @RequestParam(name = "gender", required = false) Optional<String> gender,
                                                             @RequestParam(name = "active", required = false) Optional<Boolean> active,
                                                             @RequestParam(name = "year", required = false) Optional<String> year,
                                                             @RequestParam("page") int pageIndex) {

        return studentService.getStudents(firstName,lastName,email,gender,active,year,pageIndex);
    }

    @GetMapping(value = "/course-list")
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<Page<StudentOut>> getCourseHomework(@RequestParam(name = "course") Long courseId,
                                                              @RequestParam("page") int pageIndex) {
        return studentService.getCourseStudents(courseId,pageIndex);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<AdminStudentOut> postStudent(@RequestBody @Valid StudentIn studentIn) throws ParseException {
        return studentService.saveStudent(studentIn);
    }



    /**
     * STUDENT APP METHODS
     */
    @GetMapping(value = "/out/{id}")
    public ResponseEntity<StudentOut> getStudentOut(@PathVariable(name = "id") long id) {
        Optional<Student> optStudent = studentService.findById(id);

        if (optStudent.isPresent()) {
            Student student = optStudent.get();
            StudentOut studentDto = new StudentOut();
            studentDto.setCompleteName(student.getFirstName() + " " + student.getLastName());
            studentDto.setId(student.getId());
            int pendingHomeworks = 0;
            double averageScore = 0;
            Map<String,Double> coursesScore = new HashMap<>();
            Map<String,String> homeworkMap = new HashMap<>();

            if (!student.getCourses().isEmpty()) {
                    for(CourseTeacherStudent course: student.getCourses()) {
                        if(course.getScore() != 0){
                            averageScore += course.getScore();
                            coursesScore.put(course.getCourseTeacher().getCourse().getName(),course.getScore());
                        }
                        for (StudentHomeworks homework : course.getHomeworkList()) {
                            if (homework.getHomework().getDeliveryDate().after(new Date())) {
                                pendingHomeworks++;
                                homeworkMap.put(course.getCourseTeacher().getCourse().getName(),homework.getHomework().getName());
                            }
                        }
                    }
                studentDto.setAverage(averageScore / student.getCourses().size());
                studentDto.setPendingHomeworks(pendingHomeworks);
                studentDto.setCourseNoteStudentOut(coursesScore);
                studentDto.setHomeworkListStudentOut(homeworkMap);
            }
            studentDto.setUnreadMessages(2);
            return new ResponseEntity<>(studentDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

}
