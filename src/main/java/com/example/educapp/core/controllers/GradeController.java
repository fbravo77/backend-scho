package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.GradeOut;
import com.example.educapp.core.models.dto.receive.GradeIn;
import com.example.educapp.core.models.persistence.GradeSection;
import com.example.educapp.core.models.persistence.Section;
import com.example.educapp.core.services.GradeService;
import com.example.educapp.core.services.YearService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/grade", produces = "application/json")
@CrossOrigin(origins = "*")
public class GradeController {

    private static final Logger logger = LogManager.getLogger(GradeController.class);

    @Autowired
    private GradeService gradeService;

    @Autowired
    private YearService yearServ;

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<GradeOut>> getGrades(@RequestParam(name = "name", required = false) Optional<String> name,
                                                @RequestParam(name = "active", required = false) Optional<Boolean> active,
                                                @RequestParam(name = "year", required = false) Optional<String> year,
                                                @RequestParam("page") int pageIndex) {

       return gradeService.getGrades(name,year,active,pageIndex);
    }

    @GetMapping(path = {"/{id}"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<GradeOut> getGradeById(@PathVariable(name = "id")  long id) {
        return gradeService.getGradeOutById(id);
    }

    @GetMapping(value = "/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR','ROLE_TEACHER')")
    public ResponseEntity<List<GradeOut>> getGradesList() {

        return gradeService.getGradesListAdmin();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<GradeOut> postGrade(@RequestBody @Valid GradeIn grade) {
        return gradeService.saveGrade(grade);
    }

    /**
     * METHODS FOR SAVE GRADE-SECTIONS
     */
    @PostMapping(path = "/grade-section", consumes = "application/json")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<GradeSection> postGradeSection(@RequestBody @Valid GradeSection gradeSection) {
        return new ResponseEntity<>(gradeService.save(gradeSection), HttpStatus.OK);
    }

    @GetMapping(path = {"/gradesections/{id}"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<List<Section>> findSectionsByGradeAndIsActive(@PathVariable(name = "id")  long id) {
        return gradeService.findSectionsByGradeAndIsActive(id);
    }



}
