package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.CourseOut;
import com.example.educapp.core.models.dto.outgoing.CourseTeacherOut;
import com.example.educapp.core.models.dto.receive.CourseIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.services.CourseService;
import com.example.educapp.core.services.GradeService;
import com.example.educapp.core.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping(path = "/course", produces = "application/json")
@CrossOrigin(origins = "*")
public class CourseController {

    @Autowired
    private CourseService courseServ;

    @Autowired
    private GradeService gradeServ;

    @Autowired
    private StudentService studentService;

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<CourseOut>> getCourses(@RequestParam(name = "name", required = false)Optional<String> name,
                                                      @RequestParam(name = "active",required = false)Optional<Boolean> active,
                                                      @RequestParam(name = "year",required = false)Optional<String> year,
                                                      @RequestParam("page") int pageIndex) {

       return courseServ.getCoursesAdmin(name,active,year,pageIndex);
    }

    @GetMapping(value = "/teacher")
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<Page<CourseTeacherOut>> getCoursesTeacher(@RequestParam(name = "name", required = false)Optional<String> name,
                                                                    @RequestParam(name = "active",required = false)Optional<String> active,
                                                                    Authentication authentication,
                                                                    @RequestParam("page") int pageIndex) {
        return courseServ.getCoursesTeacher(name,active,authentication.getName(),pageIndex);
    }

    @GetMapping(value = "/teacher-course")
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<CourseTeacherOut> getCourseTeacherById(@RequestParam("id") int id) {
        return courseServ.getCourseTeacherById(id);
    }

    @GetMapping(value = "/all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<List<CourseOut>> getCoursesList() {

        return courseServ.getCoursesListAdmin();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<CourseOut> postCourse(@RequestBody @Valid CourseIn course) {
        return courseServ.save(course);
    }

    @GetMapping(value = "/student-courses/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<List<CourseOut>> getStudentCoursesApp(@PathVariable(name = "id") long id) {
        Optional<Student> optStudent = studentService.findById(id);

        if (optStudent.isPresent()) {
            Student student = optStudent.get();
            List<CourseTeacherStudent> courses = student.getCourses();
            List<CourseOut> coursesOut = new ArrayList<>();
            //COURSES DTO LIST
            for (int i = 0; i < courses.size(); i++) {
                CourseOut courseOut = new CourseOut();
                CourseTeacherStudent courseTeacherStudent = courses.get(i);
                courseOut.setId(courseTeacherStudent.getId());
                courseOut.setScore(courseTeacherStudent.getScore());
                courseOut.setName(courseTeacherStudent.getCourseTeacher().getCourse().getName());
                Map <String,Double> scoreDetail = new HashMap<>();
                int pendingHomework = 0;
                double accumulatedScore = 0.0;
                for (StudentHomeworks studentHomeworks : courseTeacherStudent.getHomeworkList()) {
                    if(studentHomeworks.getScore() != 0) {
                        accumulatedScore += studentHomeworks.getScore();
                        scoreDetail.put(studentHomeworks.getHomework().getName(), studentHomeworks.getScore());
                    }
                    if(studentHomeworks.getHomework().getDeliveryDate().after(new Date())){
                        pendingHomework++;
                    }
                }
                courseOut.setAccumulatedScore(accumulatedScore);
                courseOut.setCourseNoteStudentOut(scoreDetail);
                courseOut.setPendingHomework(pendingHomework);
                coursesOut.add(courseOut);
            }
            return new ResponseEntity<>(coursesOut, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
