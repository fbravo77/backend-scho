package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.HomeworkOut;
import com.example.educapp.core.models.dto.receive.CheckHomeworkInList;
import com.example.educapp.core.models.dto.receive.HomeworkIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.services.HomeworkService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/homework", produces = "application/json")
@CrossOrigin(origins = "*")
public class HomeworkController {

    private static final Logger logger = LogManager.getLogger(HomeworkController.class);

    @Autowired
    private HomeworkService homeworkService;

    /**
     * ADMIN METHODS
     */
    @GetMapping(value = "/course-list")
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<Page<HomeworkOut>> getCourseHomework(@RequestParam(name = "course") Long courseId,
                                                                    Authentication authentication,
                                                                    @RequestParam("page") int pageIndex) {
        return homeworkService.getCourseHomework(courseId,pageIndex);
    }

    @PostMapping(path = "/new", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<HomeworkOut> postHomework(@RequestBody @Valid HomeworkIn homework) {
        return homeworkService.postHomework(homework);
    }

    @GetMapping(path = "/{homeworkId}")
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity<HomeworkOut> getHomework(@PathVariable(name = "homeworkId") Long homeworkId) {
        return homeworkService.getHomeworkById(homeworkId);
    }

    @PostMapping(path = "/grade", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_TEACHER')")
    public ResponseEntity postGradeHomework(@RequestBody CheckHomeworkInList gradehomework) {
        return homeworkService.checkHomework(gradehomework);
    }

    /**
     * STUDENT METHODS
    */

    @GetMapping(path = {"/{name}/{detail}", "list/{score}"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<List<HomeworkOut>> getHomework(@PathVariable(name = "name", required = false) Optional<String> name, @PathVariable(name = "detail", required = false) Optional<String> detail, @PathVariable(name = "score", required = false) Optional<Double> score) {
        List<Homework> optHomework;

        if (name.isPresent() || detail.isPresent())
            optHomework = homeworkService.findByNameLikeOrDetailLike(name.orElse(""),detail.orElse(""));
        else {
            optHomework = homeworkService.findByScore(score.orElse(0.0));
        }
        if (!optHomework.isEmpty()) {
            List<HomeworkOut> optHomeworkOut = new ArrayList<>();
            for(Homework currentHomework : optHomework){
                HomeworkOut homeworkOut = new HomeworkOut();
                homeworkOut.setName(currentHomework.getName());
                homeworkOut.setCourse(currentHomework.getCourseTeacher().getCourse().getName());
                homeworkOut.setDetail(currentHomework.getDetail());
                homeworkOut.setDeliveryDate(currentHomework.getDeliveryDate());
                homeworkOut.setTeacherName(currentHomework.getCourseTeacher().getTeacher().getFirstName() + currentHomework.getCourseTeacher().getTeacher().getLastName());
                optHomeworkOut.add(homeworkOut);
            }
            return new ResponseEntity<>(optHomeworkOut, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
