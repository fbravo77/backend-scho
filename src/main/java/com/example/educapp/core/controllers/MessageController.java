package com.example.educapp.core.controllers;


import com.example.educapp.core.services.GradeService;
import com.example.educapp.core.services.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/general-messages", produces = "application/json")
@CrossOrigin(origins = "*")
public class MessageController {

    private static final Logger logger = LogManager.getLogger(MessageController.class);

    @Autowired
    private MessageService messageServ;

    @Autowired
    private GradeService gradeServ;
/*
    @GetMapping(path = {"/{gradeSection}", ""})
    public ResponseEntity<List<MessageOut>> getMessages(@PathVariable(name = "gradeSection",
            required = false)
                                                                Optional<Long> gradeSectionId) {
        List<MessageOut> messageOuts = new ArrayList<>();
        List<Message> messages = new ArrayList<>();
        if (gradeSectionId.isPresent()) {
            Optional<GradeSection> gradeSection = gradeServ.findGradeSectionById(gradeSectionId.get());
            if (gradeSection.isPresent())
                messages = messageServ.findByGradeSectionOrGradeSectionIsNullAndDateAfter(gradeSection.get(), new Date());
        } else
            messages = messageServ.findByDateAfterAndGradeSectionIsNull(new Date());

        if (!messages.isEmpty()) {
            for (Message currentMessage : messages) {
                MessageOut messageOut = new MessageOut();
                messageOut.setDate(currentMessage.getDate());
                messageOut.setMessage(currentMessage.getMessage());
                messageOut.setSubject(currentMessage.getSubject());
                messageOuts.add(messageOut);
            }
            return new ResponseEntity<>(messageOuts, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/create", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Message> postMessage(@RequestBody @Valid MessageIn messageIn) {

        Message message = new Message();
        if (messageIn.getGradeSectionId() != 0L) {
            Optional<GradeSection> gradeSection = gradeServ.findGradeSectionById(messageIn.getGradeSectionId());
            if (gradeSection.isPresent())
                message.setGradeSection(gradeSection.get());
        }
        message.setDate(new Date());
        message.setMessage(messageIn.getMessage());
        message.setSubject(messageIn.getSubject());
        return new ResponseEntity<>(messageServ.save(message), HttpStatus.OK);
    }
*/

}
