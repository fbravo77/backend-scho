package com.example.educapp.core.controllers;


import com.example.educapp.core.models.persistence.Year;
import com.example.educapp.core.services.YearService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "/year", produces = "application/json")
@CrossOrigin(origins = "*")

public class YearController {

    private static final Logger logger = LogManager.getLogger(YearController.class);

    @Autowired
    private YearService yearService;

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<Year>> getYears(@RequestParam(name="year", required = false) Optional<Integer> year,
                                        @RequestParam(name = "active", required = false) Optional<Boolean> active,
                                        @RequestParam("page") int pageIndex) {
        return yearService.getYears(year,active,pageIndex);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Year> postYear(@RequestBody @Valid Year year) {
        return yearService.save(year);
    }

}
