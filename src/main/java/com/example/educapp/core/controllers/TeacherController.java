package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.TeacherOut;
import com.example.educapp.core.models.dto.receive.TeacherIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.services.TeacherService;
import com.example.educapp.core.services.YearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Optional;

@RestController
@RequestMapping(path = "/teacher", produces = "application/json")
@CrossOrigin(origins = "*")

public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private YearService yearServ;

    @GetMapping(path = {"/{id}"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<TeacherOut> getTeacherById(@PathVariable(name = "id")  long id) {
        return teacherService.getTeacherOutById(id);
    }

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<TeacherOut>> getTeachers(@RequestParam(name = "firstName", required = false) Optional<String> firstName,
                                                        @RequestParam(name = "lastName", required = false) Optional<String> lastName,
                                                        @RequestParam(name = "email", required = false) Optional<String> email,
                                                        @RequestParam(name = "gender", required = false) Optional<String> gender,
                                                        @RequestParam(name = "active", required = false) Optional<Boolean> active,
                                                        @RequestParam(name = "year", required = false) Optional<String> year,
                                                        @RequestParam("page") int pageIndex) {

        return teacherService.getTeachers(firstName,lastName,email,gender,active,year,pageIndex);
    }


    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<TeacherOut> postTeacher(@RequestBody @Valid TeacherIn teacherIn) throws ParseException {
        return teacherService.saveTeacher(teacherIn);
    }

    //ASSIGN COURSES TO TEACHER
    @PostMapping(path = "/assign-course", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<CourseTeacher> postTeacher(@RequestBody @Valid CourseTeacher courseTeacher) {
        return new ResponseEntity<>(teacherService.saveCourses(courseTeacher), HttpStatus.OK);
    }

    @PostMapping(path = "/grade-homework", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StudentHomeworks> postStudentHomework(@RequestBody @Valid StudentHomeworks gradeHomework) {
        return new ResponseEntity<>(teacherService.gradeHomework(gradeHomework), HttpStatus.OK);
    }
}
