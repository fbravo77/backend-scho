package com.example.educapp.core.controllers;

import com.example.educapp.core.models.dto.outgoing.SectionOut;
import com.example.educapp.core.models.dto.receive.SectionIn;
import com.example.educapp.core.services.SectionService;
import com.example.educapp.core.services.YearService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/section", produces = "application/json")
@CrossOrigin(origins = "*")
public class SectionController {

    @Autowired
    private SectionService sectionService;

    @Autowired
    private YearService yearServ;

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<Page<SectionOut>> getSections(@RequestParam(name = "name", required = false) Optional<String> name,
                                                    @RequestParam(name = "active", required = false) Optional<Boolean> active,
                                                    @RequestParam(name = "year", required = false) Optional<String> year,
                                                    @RequestParam("page") int pageIndex) {

        return sectionService.getSectionsPage(name,active,year,pageIndex);
    }

    @GetMapping(path = {"/list"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<List<SectionOut>> getSectionList() {
        return sectionService.getSectionListAdmin();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<SectionOut> postSection(@RequestBody @Valid SectionIn section) {
        return sectionService.save(section);
    }

    @GetMapping(path = {"/{id}"})
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_COORDINATOR')")
    public ResponseEntity<SectionOut> getSectionById(@PathVariable(name = "id")  long id) {
        return sectionService.getSectionOutById(id);
    }
}
