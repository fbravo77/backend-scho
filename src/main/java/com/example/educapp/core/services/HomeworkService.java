package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.HomeworkOut;
import com.example.educapp.core.models.dto.receive.CheckHomeworkInList;
import com.example.educapp.core.models.dto.receive.HomeworkIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.repositories.CourseTeacherStudentRepository;
import com.example.educapp.core.repositories.HomeworkRepository;
import com.example.educapp.core.repositories.StudentHomeworkRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HomeworkService {

    @Autowired
    private HomeworkRepository homeworkRep;

    @Autowired
    private StudentHomeworkRepository studentHomeworkRep;

    @Autowired
    private
    CourseTeacherStudentRepository courseTeacherStudentRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private CourseService courseServ;

    @Autowired
    private TeacherService teacherServ;

    /**
     * REST METHODS
     */
    public ResponseEntity<HomeworkOut> getHomeworkById(Long homeworkId) {

        //Obtain course
        Optional <Homework> homework = homeworkRep.findById(homeworkId);
        if(!homework.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
                HomeworkOut homeworkOut = new HomeworkOut(
                        homework.get().getId(),
                        homework.get().getName(),
                        homework.get().getDetail(),
                        homework.get().getScore(),
                        homework.get().getDeliveryDate(),
                        null, null, homework.get().getCourseTeacher().id);
            return new ResponseEntity<>(homeworkOut, HttpStatus.OK);
    }

    public ResponseEntity<Page<HomeworkOut>> getCourseHomework(Long courseId, int pageIndex) {

        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("id").descending());

        Page homeworkPage;
        Page homeworkOutPage;
        //Obtain course
        Optional <CourseTeacher> courseTeacher = courseServ.getCourseTeacher(courseId);
        if(!courseTeacher.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        //Obtain year
        Optional<Year> currentYear = yearServ.findByIsActive(true);
        QHomework qHomework = QHomework.homework;
        //PARA QUE BUSQUE TODOS
        BooleanExpression query = Expressions.asBoolean(true).isTrue();
        query = query.and(qHomework.courseTeacher.eq(courseTeacher.get()));

        homeworkPage = homeworkRep.findAll(query,page);

        if (homeworkPage.getContent().size() > 0) {
            List<HomeworkOut> outHomework = new ArrayList<>();
            for(Homework currentHomework: (List<Homework>) homeworkPage.getContent()){
                HomeworkOut newHomeworkOut = new HomeworkOut(
                        currentHomework.getId(),
                        currentHomework.getName(),
                        currentHomework.getDetail(),
                        currentHomework.getScore(),
                        currentHomework.getDeliveryDate(),
                        null, null, currentHomework.getCourseTeacher().getId());

                outHomework.add(newHomeworkOut);
            }
            homeworkOutPage = new PageImpl<>(outHomework, page, homeworkPage.getTotalElements());
            return new ResponseEntity<>(homeworkOutPage, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<HomeworkOut> postHomework(HomeworkIn homework) {

        Homework homeworkModel = new Homework();
        if (homework.getCourseTeacherId() != 0L) {
            Optional<CourseTeacher> courseTeacher = teacherServ.findCourseTeacherById(homework.getCourseTeacherId());
            if (courseTeacher.isPresent())
                homeworkModel.setCourseTeacher(courseTeacher.get());
        }
        homeworkModel.setDeliveryDate(homework.getDeliveryDate());
        homeworkModel.setDetail(homework.getDetail());
        homeworkModel.setName(homework.getName());
        homeworkModel.setScore(homework.getScore());

        homeworkModel = save(homeworkModel);
        if(homeworkModel.getId() != 0L){
            HomeworkOut homeworkOut = new HomeworkOut(homeworkModel.getId(),homeworkModel.getName(),homeworkModel.getDetail(),
                                                      homeworkModel.getScore(),homeworkModel.getDeliveryDate(),"",""
                    ,homeworkModel.getCourseTeacher().getId());

            return new ResponseEntity<>(homeworkOut, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Transactional
    public ResponseEntity checkHomework(CheckHomeworkInList checkHomework) {

        for(int i=0; i < checkHomework.getCheckHomeworkInList().size(); i++) {
            StudentHomeworks homeworkModel = new StudentHomeworks();
            CheckHomeworkInList.CheckHomeworkIn currentHomeworkIn = checkHomework.getCheckHomeworkInList().get(i);

            if (checkHomework.getCheckHomeworkInList().get(i).getHomeworkId() != 0L) {
                Optional<Homework> homework = findById(currentHomeworkIn.getHomeworkId());
                if (homework.isPresent())
                    homeworkModel.setHomework(homework.get());
            }
            Optional<CourseTeacherStudent> courseTeacherStudent = Optional.empty();
            if (currentHomeworkIn.getCourseTeacherStudentId() != 0L) {
                courseTeacherStudent = courseTeacherStudentRep.findById(currentHomeworkIn.getCourseTeacherStudentId());
                if (courseTeacherStudent.isPresent())
                    homeworkModel.setStudentCourse(courseTeacherStudent.get());
            }
            homeworkModel.setScore(currentHomeworkIn.getScore());
            homeworkModel.setAnnotation(currentHomeworkIn.getAnnotation());
            homeworkModel = studentHomeworkRep.save(homeworkModel);

            if (homeworkModel.getId() != 0L && courseTeacherStudent.isPresent()) {
                courseTeacherStudent.get().setScore(courseTeacherStudent.get().getScore() + currentHomeworkIn.getScore());
                courseTeacherStudentRep.save(courseTeacherStudent.get());
            }
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * INTERNAL METHODS
     */

    public Optional<Homework> findById(long id){ return homeworkRep.findById(id);}

    public List<Homework> findByNameLikeOrDetailLike(String name,String detail){
        return homeworkRep.findByNameLikeOrDetailLike(name, detail);
    }

    public List<Homework> findByScore(double score){
        return homeworkRep.findByScore(score);
    }

    public Homework save (Homework homework){
        return homeworkRep.save(homework);
    }

}
