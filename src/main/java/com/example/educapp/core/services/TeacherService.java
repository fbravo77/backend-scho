package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.TeacherOut;
import com.example.educapp.core.models.dto.receive.TeacherIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.repositories.*;
import com.example.educapp.security.entity.Role;
import com.example.educapp.security.entity.User;
import com.example.educapp.security.enums.RoleName;
import com.example.educapp.security.service.RoleService;
import com.example.educapp.security.service.UserService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.educapp.core.models.persistence.Teacher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TeacherService {


    @Autowired
    private TeacherRepository teacherRep;

    @Autowired
    private CourseTeacherRepository courseTeacherRep;

    @Autowired
    private HomeworkRepository homeworkRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private GradeService gradeServ;

    @Autowired
    private SectionService sectionServ;

    @Autowired
    private CourseService courseServ;

    @Autowired
    private StudentHomeworkRepository studentHomeworkRep;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;

    public List<Teacher> findByIsActiveAndYear(boolean active, Year year) {
        return null; //teacherRep.findByIsActiveAndYear(active, year);
    }

    public Optional<Teacher> findByContactMail (boolean active, Year year,String contactEmail) {
        return teacherRep.findByIsActiveAndYearAndContactEmail(active, year,contactEmail);
    }

    /*REST METHODS*/

    public ResponseEntity<TeacherOut> getTeacherOutById(long id) {
        Optional<Teacher> teacher = teacherRep.findById(id);
        if(teacher.isPresent()) {
            Mapper mapper = DozerBeanMapperBuilder.buildDefault();
            TeacherOut teacherOut = mapper.map(teacher.get(), TeacherOut.class);
            teacherOut.setYear(String.valueOf(teacher.get().getYear().getYear()));
            return new ResponseEntity<>(teacherOut, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Page<TeacherOut>> getTeachers(Optional<String> firstName,
                                                        Optional<String> lastName,
                                                        Optional<String> email,
                                                        Optional<String> gender,
                                                        Optional<Boolean> active,
                                                        Optional<String> year,
                                                        int pageIndex) {
        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("firstName").descending());

        Page<Teacher> teacherPage;
        Page<TeacherOut> teacherOutPage;
        Optional<Year> currentYear = Optional.empty();
        if (year.isPresent() && !year.get().isEmpty()) {
            currentYear = yearServ.findByYear(Integer.parseInt(year.get()));
            if (!currentYear.isPresent()) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
        QTeacher qTeacher = QTeacher.teacher;
        BooleanExpression query = Expressions.asBoolean(true).isTrue();
        if(firstName.isPresent() && !firstName.get().equals(""))
            query = query.and(qTeacher.firstName.equalsIgnoreCase(firstName.get()));
        if(lastName.isPresent() && !lastName.get().equals(""))
            query = query.and(qTeacher.lastName.equalsIgnoreCase(lastName.get()));
        if(email.isPresent() && !email.get().equals(""))
            query = query.and(qTeacher.contactEmail.equalsIgnoreCase(email.get()));
        if(gender.isPresent() && !gender.get().equals(""))
            query = query.and(qTeacher.gender.equalsIgnoreCase(gender.get()));
        if(active.isPresent())
            query = query.and(qTeacher.isActive.eq(active.get()));
        if(currentYear.isPresent())
            query = query.and(qTeacher.year.eq(currentYear.get()));

        teacherPage = teacherRep.findAll(query,page);

        if (teacherPage.getContent().size() > 0) {
            List<TeacherOut> outTeachers = new ArrayList<>();
            for (Teacher currentTeacher : teacherPage.getContent()) {
                Mapper mapper = DozerBeanMapperBuilder.buildDefault();
                TeacherOut teacherOut = mapper.map(currentTeacher, TeacherOut.class);
                teacherOut.setYear(String.valueOf(currentTeacher.getYear().getYear()));
                outTeachers.add(teacherOut);
            }
            teacherOutPage = new PageImpl<>(outTeachers, page, teacherPage.getTotalElements());
            return new ResponseEntity<>(teacherOutPage, HttpStatus.OK);
        }
        else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<TeacherOut> saveTeacher(TeacherIn teacherIn) throws ParseException {
        Optional<Year> year = yearServ.findByIsActive(true);
        if (!year.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        User user = new User(teacherIn.getContactEmail(),passwordEncoder.encode("123456"),true);
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getRoleByName(RoleName.ROLE_TEACHER).get());

        user.setRoles(roles);
        userService.save(user);

        //TODO SEND PASSWORD POR CORREO
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        Teacher teacher = mapper.map(teacherIn, Teacher.class);
        teacher.setYear(year.get());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = formatter.parse(teacherIn.getDateOfBirth());
        teacher.setDateBirth(date);
        teacher.setUser(user);
        teacherRep.save(teacher);
            TeacherOut teacherOut = mapper.map(teacher, TeacherOut.class);
            if(teacher.id != null) {
                Optional<Grade> currentGradeOpt;
                Optional<Course> currentCourseOpt;
                Optional<Section> currentSectionOpt;
                Optional<GradeSection> currentGradeSectionOpt;
                for (TeacherIn.CourseTeacherIn currentCourseTeacher : teacherIn.getCourses()) {
                    currentGradeOpt = gradeServ.findById(currentCourseTeacher.getGradeId());
                    currentCourseOpt = courseServ.getCourseById(currentCourseTeacher.getCourseId());
                    currentSectionOpt = sectionServ.findById(currentCourseTeacher.getSectionId());
                    currentGradeSectionOpt = gradeServ.findFirstByGradeAndSection(currentGradeOpt.get(),currentSectionOpt.get());
                    CourseTeacher courseTeacher = new CourseTeacher(currentGradeSectionOpt.get(),teacher,currentCourseOpt.get());
                    courseTeacherRep.save(courseTeacher);
                }
                return new ResponseEntity<>(teacherOut, HttpStatus.OK);
            }
            else
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * COURSES
     * */
    public CourseTeacher saveCourses (CourseTeacher courseTeacher){
        return courseTeacherRep.save(courseTeacher) ;
    }

    public Optional<CourseTeacher> findCourseTeacherById(long id){ return courseTeacherRep.findById(id);}

    /**
     * HOMEWORKS
     * */
    public StudentHomeworks gradeHomework (StudentHomeworks studentHomeworks){
        return studentHomeworkRep.save(studentHomeworks) ;
    }

}
