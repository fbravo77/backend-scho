package com.example.educapp.core.services;

import com.example.educapp.core.models.persistence.Year;
import com.example.educapp.core.repositories.YearRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Gestiona la capa de negocio de los consumibles.
 *
 * @author Francisco Bravo
 */
@Service
public class YearService {

    @Autowired
    private YearRepository yearRep;

    public Optional<Year> findById(Long id){
        return yearRep.findById(id);
    }

    public Optional<Year> findByYear(int year){
        return yearRep.findByYear(year);
    }

    public Optional<Year> findByIsActive(boolean active){
        return yearRep.findByIsActive(active);
    }

    public ResponseEntity<Page<Year>> getYears (Optional<Integer> year,Optional<Boolean> active,int pageIndex){

        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("year").ascending());

        Page pageYear;
        if(year.isPresent() && active.isPresent())
            pageYear = yearRep.findByYearAndIsActive(year.get(), active.get(),page);
        else if (active.isPresent())
            pageYear = yearRep.findByIsActive(active.get(),page);
         else if (year.isPresent())
            pageYear = yearRep.findByYear(year.get(),page);
         else
            pageYear = yearRep.findAll(page);

        if (pageYear.getContent().size() > 0) {
            return new ResponseEntity<>(pageYear, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Year> save (Year year){
        //Only one active year at time
        if(year.isActive())
        {
            if(!yearRep.findByIsActive(true, PageRequest.of(
                    0, 10)).getContent().isEmpty())
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(yearRep.save(year), HttpStatus.OK);
    }


}
