package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.SectionOut;
import com.example.educapp.core.models.dto.receive.SectionIn;
import com.example.educapp.core.models.persistence.Section;
import com.example.educapp.core.models.persistence.Year;
import com.example.educapp.core.repositories.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Gestiona la capa de negocio de los consumibles.
 *
 * @author Francisco Bravo
 */
@Service
public class SectionService {

    @Autowired
    private SectionRepository sectionRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private GradeService gradeServ;

    public ResponseEntity<SectionOut> getSectionOutById(long id) {
        Optional<Section> section = sectionRep.findById(id);
        if (section.isPresent())
            return new ResponseEntity<>(new SectionOut(section.get().getId(), section.get().getName(), section.get().getYear().getYear(), section.get().isActive()), HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Page<SectionOut>> getSectionsPage(Optional<String> name, Optional<Boolean> active, Optional<String> year, int pageIndex) {
        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("name").descending());

        Page<Section> sectionPage;
        Page<SectionOut> sectionOutPage;
        Optional<Year> currentYear = Optional.empty();
        if (year.isPresent() && !year.get().isEmpty()) {
            currentYear = yearServ.findByYear(Integer.parseInt(year.get()));
            if (!currentYear.isPresent()) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
        if (currentYear.isPresent() && active.isPresent() && name.isPresent() && !name.get().isEmpty())
            sectionPage = sectionRep.findByNameContainingIgnoreCaseAndIsActiveAndYear(name.get(), active.get(), currentYear.get(), page);

        else if (!currentYear.isPresent() && active.isPresent() && name.isPresent() && !name.get().isEmpty())
            sectionPage = sectionRep.findByNameContainingIgnoreCaseAndIsActive(name.get(), active.get(), page);
        else if (currentYear.isPresent() && active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            sectionPage = sectionRep.findByIsActiveAndYear(active.get(), currentYear.get(), page);

        else if (!currentYear.isPresent() && !active.isPresent() && name.isPresent() && !name.get().isEmpty())
            sectionPage = sectionRep.findByNameContainingIgnoreCase(name.get(), page);

        else if (!currentYear.isPresent() && active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            sectionPage = sectionRep.findByIsActive(active.get(), page);

        else if (currentYear.isPresent() && !active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            sectionPage = sectionRep.findByYear(currentYear.get(), page);

        else if (currentYear.isPresent() && !active.isPresent() && name.isPresent() && !name.get().isEmpty())
            sectionPage = sectionRep.findByNameContainingIgnoreCaseAndYear(name.get(), currentYear.get(), page);
        else
            sectionPage = sectionRep.findAll(page);

        if (sectionPage.getContent().size() > 0) {
            List<SectionOut> outSections = new ArrayList<>();
            for (Section currentSection : sectionPage.getContent()) {
                SectionOut newSectionOut = new SectionOut(currentSection.getId(), currentSection.getName(), currentSection.getYear().getYear(), currentSection.isActive());
                outSections.add(newSectionOut);
            }
            sectionOutPage = new PageImpl<>(outSections, page, sectionPage.getTotalElements());
            return new ResponseEntity<>(sectionOutPage, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public List<Section> findByIsActiveAndYear(boolean active, Year year) {
        return sectionRep.findByIsActiveAndYear(active, year);
    }

    public Optional<Section> findById(long id) {
        return sectionRep.findById(id);
    }

    public ResponseEntity<SectionOut> save(SectionIn sectionIn) {
        Optional<Year> year = yearServ.findByIsActive(true);
        if (!year.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Section sectionModel = new Section();
        sectionModel.setActive(sectionIn.isActive());
        sectionModel.setYear(year.get());
        sectionModel.setName(sectionIn.getName());
        sectionModel.setId(sectionIn.getId());
        if (sectionRep.save(sectionModel).id != null) {
            SectionOut sectionOut = new SectionOut(
                    sectionModel.id,
                    sectionModel.getName(),
                    sectionModel.getYear().getYear(),
                    sectionModel.isActive()
            );
            return new ResponseEntity<>(sectionOut, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<List<SectionOut>> getSectionListAdmin() {
        Optional<Year> year = yearServ.findByIsActive(true);
        if (year.isPresent()) {

            List sectionOut = sectionRep.findByIsActiveAndYear(true, year.get());
            if (!sectionOut.isEmpty())
                return new ResponseEntity<>(sectionOut, HttpStatus.OK);
            else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        } else
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
