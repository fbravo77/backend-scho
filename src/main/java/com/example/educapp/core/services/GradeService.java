package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.GradeOut;
import com.example.educapp.core.models.dto.receive.GradeIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.repositories.GradeRepository;
import com.example.educapp.core.repositories.GradeSectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Gestiona la capa de negocio de los consumibles.
 *
 * @author Francisco Bravo
 */
@Service
public class GradeService {

    @Autowired
    private GradeRepository gradeRep;

    @Autowired
    private GradeSectionRepository gradeSectionRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private CourseService courseServ;

    @Autowired
    private SectionService sectionServ;

    public Optional<Grade> findById(long id){ return gradeRep.findById(id);}

    public ResponseEntity<GradeOut> getGradeOutById(long id){
        Optional <Grade> grade = gradeRep.findById(id);
        if(grade.isPresent()) {
            List<GradeSection> currentGradeSectionList = gradeSectionRep.findByGradeAndIsActive(grade.get(),true);
            String gradeSectionArr = "";
            for (GradeSection gradeSection : currentGradeSectionList){
                    gradeSectionArr += gradeSection.getSection().id + ",";
            }
            return  new ResponseEntity<>(new GradeOut(grade.get().getId(),
                    grade.get().getName(),grade.get().getYear().getYear(),grade.get().isActive(),grade.get().getCourses(),
                    gradeSectionArr != "" ? gradeSectionArr.split(","): null),HttpStatus.OK);
        } else{
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Page<GradeOut>> getGrades (Optional<String> name, Optional<String> year,Optional<Boolean> active,int pageIndex) {

        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("name").descending());

        Page<Grade> gradePage;
        Page<GradeOut> gradeOutPage;
        Optional<Year> currentYear = Optional.empty();
        if(year.isPresent() && !year.get().isEmpty()){
            currentYear = yearServ.findByYear(Integer.parseInt(year.get()));
        }
        if(currentYear.isPresent() && active.isPresent() && name.isPresent() && !name.get().isEmpty())
            gradePage = gradeRep.findByNameLikeAndIsActiveAndYear(name.get(),active.get(),currentYear.get(),page);

        else if (!currentYear.isPresent() && active.isPresent() && name.isPresent() && !name.get().isEmpty())
            gradePage = gradeRep.findByNameLikeAndIsActive(name.get(), active.get(), page);

        else if (currentYear.isPresent() && active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            gradePage = gradeRep.findByYearAndIsActive(currentYear.get(),active.get(),page);

        else if(!currentYear.isPresent() && !active.isPresent() && name.isPresent() && !name.get().isEmpty())
            gradePage = gradeRep.findByNameLike(name.get(),page);

        else if(!currentYear.isPresent() && active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            gradePage = gradeRep.findByIsActive(active.get(),page);

        else if(currentYear.isPresent() && !active.isPresent() && (!name.isPresent() || name.get().isEmpty()))
            gradePage = gradeRep.findByYear(currentYear.get(),page);

        else if(currentYear.isPresent() && !active.isPresent() && name.isPresent()  && !name.get().isEmpty())
            gradePage = gradeRep.findByNameLikeAndYear(name.get(),currentYear.get(),page);
        else
            gradePage = gradeRep.findAll(page);

        if (gradePage.getContent().size() > 0) {
            List<GradeOut> outGrades = new ArrayList<>();
            for(Grade currentGrade: gradePage.getContent()){
                GradeOut newGradeOut = new GradeOut(currentGrade.getId(),currentGrade.getName(),currentGrade.getYear().getYear(),currentGrade.isActive(),currentGrade.getCourses(),null);
                outGrades.add(newGradeOut);
            }
            gradeOutPage = new PageImpl<>(outGrades, page, gradePage.getTotalElements());
            return new ResponseEntity<>(gradeOutPage, HttpStatus.OK);
        }

        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public  ResponseEntity<GradeOut> saveGrade (GradeIn grade){
        Optional<Year> year = yearServ.findByIsActive(true);
        if(!year.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        List<Course> courses = new ArrayList<>();
        for(String courseId : grade.getCourses().split(",")){
            Optional<Course> course = courseServ.getCourseById(Long.parseLong(courseId));
            if(course.isPresent()){
                courses.add(course.get());
            }
        }
        Grade gradeModel = new Grade();
        gradeModel.setActive(grade.isActive());
        gradeModel.setYear(year.get());
        gradeModel.setName(grade.getName());
        gradeModel.setId(grade.getId());
        gradeModel.setCourses(courses);
        if(gradeRep.save(gradeModel).id != null){

            GradeOut gradeOut = new GradeOut(
                    gradeModel.id,
                    gradeModel.getName(),
                    gradeModel.getYear().getYear(),
                    gradeModel.isActive(),
                    gradeModel.getCourses(),null
            );
            //Reviso si no viene la seccion y ya estaba previamente, ponerla inactiva
            List<GradeSection> currentGradeSectionList = gradeSectionRep.findByGradeAndIsActive(gradeModel,true);
            for(GradeSection currentGradeSection : currentGradeSectionList){
                currentGradeSection.setActive(false);
                gradeSectionRep.save(currentGradeSection);
            }

            for(String sectionId : grade.getSections().split(",")){
                Optional<Section> section = sectionServ.findById(Long.parseLong(sectionId));
                if(section.isPresent()){
                    //GUARDO LAS SECCIONES
                    List<GradeSection> gradeSectionList = gradeSectionRep.findByGradeAndSection(gradeModel,section.get());
                    if(gradeSectionList.isEmpty()) {
                        GradeSection gradeSection = new GradeSection();
                        gradeSection.setGrade(gradeModel);
                        gradeSection.setSection(section.get());
                        gradeSection.setActive(true);
                        gradeSectionRep.save(gradeSection);
                    }
                    else{
                        GradeSection gradeSection = gradeSectionList.get(0);
                        gradeSection.setActive(true);
                        gradeSectionRep.save(gradeSection);
                    }
                }
            }
            return new ResponseEntity<>(gradeOut, HttpStatus.OK);
        }
        else
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<List<GradeOut>> getGradesListAdmin(){
        Optional<Year> year = yearServ.findByIsActive(true);
        if(!year.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        List<GradeOut> optGradeOut =  new ArrayList<>();
        List<Grade> optGrade;
        optGrade = gradeRep.findByYearAndIsActive(year.get(),true);
        for(Grade grade : optGrade){
            GradeOut currentGradeOut = new GradeOut(grade.getId(),grade.getName(),1,true,grade.getCourses(),null);
            optGradeOut.add(currentGradeOut);
        }
        return new ResponseEntity<>(optGradeOut, HttpStatus.OK);
    }

    //METHODS TO ASSIGN SECTIONS TO GRADES
    public ResponseEntity<List<Section>> findSectionsByGradeAndIsActive(long gradeId) {

        Optional<Grade> grade = gradeRep.findById(gradeId);
        List<GradeSection> gradeSection = gradeSectionRep.findByGradeAndIsActive(grade.get(), true);
        List<Section> sections = new ArrayList<>();
        for (GradeSection currentGradeSection : gradeSection) {
            sections.add(currentGradeSection.getSection());
        }
        return new ResponseEntity<>(sections, HttpStatus.OK);
    }

    public GradeSection save(GradeSection gradeSection){return gradeSectionRep.save(gradeSection);}

    public Optional<GradeSection> findFirstByGradeAndSection(Grade grade, Section section) {
        return gradeSectionRep.findFirstByGradeAndSection(grade,section);
    }
}
