package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.AdminStudentOut;
import com.example.educapp.core.models.dto.outgoing.StudentOut;
import com.example.educapp.core.models.dto.receive.StudentIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.repositories.CourseTeacherRepository;
import com.example.educapp.core.repositories.CourseTeacherStudentRepository;
import com.example.educapp.core.repositories.StudentRepository;
import com.example.educapp.security.entity.Role;
import com.example.educapp.security.entity.User;
import com.example.educapp.security.enums.RoleName;
import com.example.educapp.security.service.RoleService;
import com.example.educapp.security.service.UserService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Gestiona la capa de negocio de los consumibles.
 *
 * @author Francisco Bravo
 */
@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRep;

    @Autowired
    private
    CourseTeacherStudentRepository courseTeacherStudentRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private GradeService gradeServ;

    @Autowired
    private SectionService sectionServ;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;

    @Autowired
    private CourseService courseServ;

    @Autowired
    private CourseTeacherRepository courseTeacherRep;
/**
 * ADMIN METHODS
 */
public ResponseEntity<Page<StudentOut>> getCourseStudents(Long courseId, int pageIndex) {

    PageRequest page = PageRequest.of(
            pageIndex, 10, Sort.by("id").descending());

    Page studentPage;
    Page studentOutPage;
    //Obtain course
    Optional <CourseTeacher> courseTeacher = courseServ.getCourseTeacher(courseId);
    if(!courseTeacher.isPresent()){
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
    //Obtain year
    Optional<Year> currentYear = yearServ.findByIsActive(true);
    QCourseTeacherStudent qCourseTeacherStudent = QCourseTeacherStudent.courseTeacherStudent;
    //PARA QUE BUSQUE TODOS
    BooleanExpression query = Expressions.asBoolean(true).isTrue();
    query = query.and(qCourseTeacherStudent.courseTeacher.eq(courseTeacher.get()));

    studentPage = courseTeacherStudentRep.findAll(query,page);

    if (studentPage.getContent().size() > 0) {
        List<StudentOut> outStudent = new ArrayList<>();
        for(CourseTeacherStudent currentStudent: (List<CourseTeacherStudent>) studentPage.getContent()){
            StudentOut newStudentOut = new StudentOut(
                    currentStudent.getId(),
                    currentStudent.getStudent().getFirstName() + " " + currentStudent.getStudent().getLastName(),
                    currentStudent.getScore(),
                    -1,
                    -1,
                    null, null);

            outStudent.add(newStudentOut);
        }
        studentOutPage = new PageImpl<>(outStudent, page, studentPage.getTotalElements());
        return new ResponseEntity<>(studentOutPage, HttpStatus.OK);
    }
    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
}

    /**
     *
     * @param id
     * @return
     */

    public Optional<Student> findById(Long id) {
        return studentRep.findById(id);
    }

    public ResponseEntity<AdminStudentOut> saveStudent(StudentIn studentIn) throws ParseException {
        Optional<Year> year = yearServ.findByIsActive(true);
        if (!year.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Mapper mapper = DozerBeanMapperBuilder.buildDefault();
        Student student = mapper.map(studentIn, Student.class);
        student.setYear(year.get());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = formatter.parse(studentIn.getDateOfBirth());
        student.setDateBirth(date);

        User user = new User(studentIn.getContactEmail(),passwordEncoder.encode("123456"),true);
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getRoleByName(RoleName.ROLE_STUDENT).get());

        user.setRoles(roles);
        userService.save(user);

        student.setUser(user);

            Optional<Grade> currentGradeOpt;
            Optional<Section> currentSectionOpt;
            Optional<GradeSection> currentGradeSectionOpt;
            currentGradeOpt = gradeServ.findById(Long.parseLong(studentIn.getGradeAndSection().split(",")[0]));
            currentSectionOpt = sectionServ.findById(Long.parseLong(studentIn.getGradeAndSection().split(",")[1]));
            currentGradeSectionOpt = gradeServ.findFirstByGradeAndSection(currentGradeOpt.get(),currentSectionOpt.get());

            student.setGradeSection(currentGradeSectionOpt.get());
            studentRep.save(student);

            //SET COURSES
            List<CourseTeacherStudent> courseTeacherStudentList = new ArrayList<>();

            List<CourseTeacher> courseTeachers;
            QCourseTeacher qCourseTeacher = QCourseTeacher.courseTeacher;
            //PARA QUE BUSQUE TODOS
            BooleanExpression query = Expressions.asBoolean(true).isTrue();

            query = query.and(qCourseTeacher.gradeSection.eq(currentGradeSectionOpt.get()));

            courseTeachers = (List<CourseTeacher>) courseTeacherRep.findAll(query);

            for(CourseTeacher courseTeacher: courseTeachers){
                CourseTeacherStudent courseTeacherStudent = new CourseTeacherStudent();
                courseTeacherStudent.setCourseTeacher(courseTeacher);
                courseTeacherStudent.setScore(0);
                courseTeacherStudent.setStudent(student);
                courseTeacherStudentList.add(courseTeacherStudent);
            }
            student.setCourses(courseTeacherStudentList);
            studentRep.save(student);

            AdminStudentOut studentOut = mapper.map(student, AdminStudentOut.class);
            studentOut.setId(student.getId());
            studentOut.setGradeAndSection(studentIn.getGradeAndSection());

            return new ResponseEntity<>(studentOut, HttpStatus.OK);
    }

    public ResponseEntity<Page<AdminStudentOut>> getStudents(Optional<String> firstName,
                                                        Optional<String> lastName,
                                                        Optional<String> email,
                                                        Optional<String> gender,
                                                        Optional<Boolean> active,
                                                        Optional<String> year,
                                                        int pageIndex) {
        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("firstName").descending());

        Page<Student> studentPage;
        Page<AdminStudentOut> studentOutPage;
        Optional<Year> currentYear = Optional.empty();
        if (year.isPresent() && !year.get().isEmpty()) {
            currentYear = yearServ.findByYear(Integer.parseInt(year.get()));
            if (!currentYear.isPresent()) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
        QStudent qStudent = QStudent.student;
        BooleanExpression query = Expressions.asBoolean(true).isTrue();
        if(firstName.isPresent() && !firstName.get().equals(""))
            query = query.and(qStudent.firstName.equalsIgnoreCase(firstName.get()));
        if(lastName.isPresent() && !lastName.get().equals(""))
            query = query.and(qStudent.lastName.equalsIgnoreCase(lastName.get()));
        if(email.isPresent() && !email.get().equals(""))
            query = query.and(qStudent.contactEmail.equalsIgnoreCase(email.get()));
        if(gender.isPresent() && !gender.get().equals(""))
            query = query.and(qStudent.gender.equalsIgnoreCase(gender.get()));
        if(active.isPresent())
            query = query.and(qStudent.isActive.eq(active.get()));
        if(currentYear.isPresent())
            query = query.and(qStudent.year.eq(currentYear.get()));

        studentPage = studentRep.findAll(query,page);

        if (studentPage.getContent().size() > 0) {
            List<AdminStudentOut> outTeachers = new ArrayList<>();
            for (Student currentStudent : studentPage.getContent()) {
                Mapper mapper = DozerBeanMapperBuilder.buildDefault();
                AdminStudentOut studentOut = mapper.map(currentStudent, AdminStudentOut.class);

                outTeachers.add(studentOut);
            }
            studentOutPage = new PageImpl<>(outTeachers, page, studentPage.getTotalElements());
            return new ResponseEntity<>(studentOutPage, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }



    public List<Student> findByIsActiveAndYear(boolean active, Year year) {
        return studentRep.findByIsActiveAndYear(active, year);
    }

    public List<Student> findByFirstNameLikeOrLastNameLikeAndIsActiveAndYear(String firstName,String lastName, boolean active, Year year)
    {
        return studentRep.findByFirstNameLikeOrLastNameLikeAndIsActiveAndYear(firstName,lastName, active,year);
    }

    public List<Student> findByGenderAndIsActiveAndYear(String gender,boolean active, Year year) {
        return studentRep.findByGenderAndIsActiveAndYear(gender, active, year);
    }
}
