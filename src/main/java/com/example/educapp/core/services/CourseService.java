package com.example.educapp.core.services;

import com.example.educapp.core.models.dto.outgoing.CourseOut;
import com.example.educapp.core.models.dto.outgoing.CourseTeacherOut;
import com.example.educapp.core.models.dto.receive.CourseIn;
import com.example.educapp.core.models.persistence.*;
import com.example.educapp.core.repositories.CourseRepository;
import com.example.educapp.core.repositories.CourseTeacherRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRep;

    @Autowired
    private YearService yearServ;

    @Autowired
    private CourseTeacherRepository courseTeacherRep;

    @Autowired
    private TeacherService teacherServ;

    public Optional<Course> getCourseById(Long id){
        return courseRep.findById(id);
    }

    public ResponseEntity<List<CourseOut>> getCoursesListAdmin(){
        Optional<Year> year = yearServ.findByIsActive(true);
        if(!year.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        List<CourseOut> optCourseOut =  new ArrayList<>();
        List<Course> optCourse;
        optCourse = courseRep.findByYearAndIsActive(year.get(),true);
        for(Course course : optCourse){
            CourseOut currentCourseOut = new CourseOut(course.getId(),course.getName(),course.getYear().getYear(),
                    course.isActive());
            optCourseOut.add(currentCourseOut);
        }
        return new ResponseEntity<>(optCourseOut, HttpStatus.OK);
    }

    public ResponseEntity<Page<CourseOut>> getCoursesAdmin(Optional<String> name, Optional<Boolean> active,Optional<String> year,int pageIndex) {

        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("name").descending());

        Page coursePage;
        Page courseOutPage;
        //Obtain year
        Optional<Year> currentYear = Optional.empty();
        if(year.isPresent() && !year.get().isEmpty()){
            currentYear = yearServ.findByYear(Integer.valueOf(year.get()));
        }

        if ((name.isPresent() && !name.get().isEmpty()) && active.isPresent() && currentYear.isPresent())
            coursePage = courseRep.findByNameContainingIgnoreCaseAndIsActiveAndYear(name.get(), active.get(), currentYear.get(),page);
        else if ((!name.isPresent() || name.get().isEmpty()) && active.isPresent() && currentYear.isPresent())
            coursePage = courseRep.findByIsActiveAndYear(active.get(), currentYear.get(),page);
        else if ((!name.isPresent() || name.get().isEmpty()) && active.isPresent() && currentYear.isPresent())
            coursePage = courseRep.findByIsActiveAndYear(active.get(), currentYear.get(),page);
        else if ((name.isPresent() && !name.get().isEmpty()) && active.isPresent() && !currentYear.isPresent())
            coursePage = courseRep.findByNameContainingIgnoreCaseAndIsActive(name.get(),active.get(),page);
        else if ((name.isPresent() && !name.get().isEmpty()) && !active.isPresent() && currentYear.isPresent())
            coursePage = courseRep.findByNameContainingIgnoreCaseAndYear(name.get(),currentYear.get(),page);
        else if ((!name.isPresent() || name.get().isEmpty()) && currentYear.isPresent())
            coursePage = courseRep.findByYear(currentYear.get(),page);
        else if ((name.isPresent() && !name.get().isEmpty()) && !active.isPresent() && !currentYear.isPresent())
            coursePage = courseRep.findByNameContainingIgnoreCase(name.get(),page);
        else if ((!name.isPresent() || name.get().isEmpty()) && active.isPresent() && !currentYear.isPresent())
            coursePage = courseRep.findByIsActive(active.get(),page);
        else
            coursePage = courseRep.findAll(page);

        if (coursePage.getContent().size() > 0) {
            List<CourseOut> outCourse = new ArrayList<>();
            for(Course currentCourse: (List<Course>) coursePage.getContent()){
                CourseOut newCourseOut = new CourseOut(
                        currentCourse.getId(),
                        currentCourse.getName(),
                        currentCourse.getYear().getYear(),
                        currentCourse.isActive());

                outCourse.add(newCourseOut);
            }
            courseOutPage = new PageImpl<CourseOut>(outCourse, page, coursePage.getTotalElements());
            return new ResponseEntity<>(courseOutPage, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Page<CourseTeacherOut>> getCoursesTeacher(Optional<String> name, Optional<String> grade, String TeacherMail, int pageIndex) {

        PageRequest page = PageRequest.of(
                pageIndex, 10, Sort.by("id").descending());

        Page coursePage;
        Page courseOutPage;
        //Obtain year
        Optional<Year> currentYear = yearServ.findByIsActive(true);
        QCourseTeacher qCourseTeacher = QCourseTeacher.courseTeacher;
        //PARA QUE BUSQUE TODOS
        BooleanExpression query = Expressions.asBoolean(true).isTrue();

        Optional<Teacher> currentTeacher = teacherServ.findByContactMail(true,currentYear.get(),TeacherMail);
        query = query.and(qCourseTeacher.teacher.eq(currentTeacher.get()));

       // if(name.isPresent() && !name.get().equals(""))
        //    query = query.and(qCourseTeacher.course.name.equalsIgnoreCase(name.get()));

        coursePage = courseTeacherRep.findAll(query,page);

        if (coursePage.getContent().size() > 0) {
            List<CourseTeacherOut> outCourse = new ArrayList<>();
            for(CourseTeacher currentCourse: (List<CourseTeacher>) coursePage.getContent()){
                CourseTeacherOut newCourseOut = new CourseTeacherOut(
                        currentCourse.getId(),
                        currentCourse.getCourse().id,
                        currentCourse.getGradeSection().id,
                        currentCourse.getCourse().getName(),
                        currentCourse.getGradeSection().getGrade().getName(),
                        currentCourse.getGradeSection().getSection().getName());

                outCourse.add(newCourseOut);
            }
            courseOutPage = new PageImpl<CourseTeacherOut>(outCourse, page, coursePage.getTotalElements());
            return new ResponseEntity<>(courseOutPage, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    public  ResponseEntity<CourseTeacherOut> getCourseTeacherById(long id){

        Optional<CourseTeacher> courseTeacher = courseTeacherRep.findById(id);

        CourseTeacherOut newCourseOut = new CourseTeacherOut(
                courseTeacher.get().getId(),
                courseTeacher.get().getCourse().id,
                courseTeacher.get().getGradeSection().id,
                courseTeacher.get().getCourse().getName(),
                courseTeacher.get().getGradeSection().getGrade().getName(),
                courseTeacher.get().getGradeSection().getSection().getName());

        return new ResponseEntity<>(newCourseOut, HttpStatus.OK);
    }

    public ResponseEntity<CourseOut> save (CourseIn course){

        Optional<Year> year = yearServ.findByIsActive(true);
        if(!year.isPresent()){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Course courseModel = new Course();
        courseModel.setActive(course.isActive());
        courseModel.setYear(year.get());
        courseModel.setName(course.getName());
        courseModel.setId(course.getId());
        if(courseRep.save(courseModel).id != null){

            CourseOut courseOut = new CourseOut(
                    courseModel.id,
                    courseModel.getName(),
                    courseModel.getYear().getYear(),
                    courseModel.isActive());

            return new ResponseEntity<>(courseOut, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * INTERNAL METHODS
     */
    public Optional<CourseTeacher> getCourseTeacher(long id){
        return courseTeacherRep.findById(id);
    }
}
