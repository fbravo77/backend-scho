package com.example.educapp.core.services;

import com.example.educapp.core.models.persistence.GradeSection;
import com.example.educapp.core.models.persistence.Message;
import com.example.educapp.core.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRep;

    public Optional<Message> findById(long id){ return messageRep.findById(id);}

    public List<Message> findByGradeSectionOrGradeSectionIsNullAndDateAfter(GradeSection gradeSection, Date date)
    {
        return messageRep.findByGradeSectionOrGradeSectionIsNullAndDateAfter(gradeSection,date);
    }

    public List<Message> findByDateAfterAndGradeSectionIsNull(Date date)
    {
        return messageRep.findByDateAfterAndGradeSectionIsNull(date);
    }

    public Message save (Message message){
        return messageRep.save(message);
    }
}
