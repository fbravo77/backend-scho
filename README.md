# INSTALATION
 1. Create the educap schema database
 2. Insert in data base, role table
```bash
INSERT INTO educap.roles (id, role_name) VALUES (1, 'ROLE_ADMIN');
INSERT INTO educap.roles (id, role_name) VALUES (2, 'ROLE_COORDINATOR');
INSERT INTO educap.roles (id, role_name) VALUES (3, 'ROLE_TEACHER');
INSERT INTO educap.roles (id, role_name) VALUES (4, 'ROLE_PARENT');
INSERT INTO educap.roles (id, role_name) VALUES (5, 'ROLE_STUDENT');
```

 3. Use Postman and do a post request to **http://localhost:5000/auth/new** to create a basic user with **user: hola@hola.com and password: 123456**
## Configuration
1. After deploy the angular app, set years.
2. Set Courses
3. Set Sections
4. Set Grades
5. Set Teachers
6. Set Students
7. update teachers to be active